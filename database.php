<?php
class DataBase {
    
    private $link;
    private $host, $username, $password, $database;
    
    public function __construct($host, $username, $password, $database) {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        
        $this->link = mysqli_connect($this->host, $this->username, $this->password, $this->database);
    }
    
    public function getLink(){
        return $this->link;
    }
}
    
?>