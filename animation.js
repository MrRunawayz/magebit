$(document).ready(function () {
    $("#signup_button").click(function () {
        $("#white-bg").animate({left:"-420px"}, 1200);
        $("#border").animate({left:"-420px"}, 1200);

        $("#login-form").fadeOut(200);

        $("#signup-form").fadeIn(1200).delay(800);
    });


    $("#login-button").click(function () {
        $("#white-bg").animate({left:"0px"}, 1200);
        $("#border").animate({left:"0px"}, 1200);

        $("#signup-form").fadeOut(200);

        $("#login-form").fadeIn(1200).delay(100);
    });


});

$(document).ready(function(){
    $("#email").keyup(function(){

        var current = $("#email").val();
        var picture = $("#login-email-grey-pic");
        if (current == "") {
            $(picture).attr('src', 'images/ic_mail_unactive.png');
        } else {
            $(picture).attr('src', 'images/ic_mail.png');
        }
    });

    $("#password").keyup(function(){

        var current = $("#password").val();
        var picture = $("#login-password-grey-pic");
        if (current == "") {
            $(picture).attr('src', 'images/ic_lock_unactive.png');
        } else {
            $(picture).attr('src', 'images/ic_lock.png');
        }
    });

    $("#signup-name").keyup(function(){

        var current = $("#signup-name").val();
        var picture = $("#signup-user-grey-pic");
        if (current == "") {
            $(picture).attr('src', 'images/ic_user_unactive.png');
        } else {
            $(picture).attr('src', 'images/ic_user.png');
        }
    });

    $("#signup-email").keyup(function(){

        var current = $("#signup-email").val();
        var picture = $("#signup-email-grey-pic");
        if (current == "") {
            $(picture).attr('src', 'images/ic_mail_unactive.png');
        } else {
            $(picture).attr('src', 'images/ic_mail.png');
        }
    });

    $("#signup-pass").keyup(function(){

        var current = $("#signup-pass").val();
        var picture = $("#signup-password-grey-pic");
        if (current == "") {
            $(picture).attr('src', 'images/ic_lock_unactive.png');
        } else {
            $(picture).attr('src', 'images/ic_lock.png');
        }
    });

});
