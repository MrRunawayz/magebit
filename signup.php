<?php

class SignUp {
    
    private $user;
    
    public function __construct($user){
        $this->user = $user;
    }

    /* Connect to database, then get values and signup. Signup successful result is being showed in result.php */

    public function doAction(& $errors){
        $this->validateRequiredFields($errors);
        
        if(count($errors) == 0){
            $database = new Database('localhost', 'root', 'password', 'login');
            $db = $database->getLink();
            
            $name = $db->real_escape_string($this->user->getName());
            $email = $db->real_escape_string($this->user->getEmail());
            $password = $db->real_escape_string($this->user->getPassword());
            
            $this->validateDuplicate($db, $name, $email, $errors);
                
            if (count($errors) == 0) {

                $hashed_password = password_hash($password, PASSWORD_DEFAULT);

                $query = "INSERT INTO users (name, email, password) 
  			  VALUES('$name', '$email', '$hashed_password')";

                $db->query($query);
                $_SESSION['name'] = $name;
                $_SESSION['success'] = "You are signed up!";
                header('location: result.php');
            }

            $db->close();
        }
    }

    /* Empty fields checking. Show errors if necessary */
    
    private function validateRequiredFields(& $errors){
        if (empty($this->user->getName())) { 
            array_push($errors, "Name is required"); 
        }
        if (empty($this->user->getEmail())) { 
            array_push($errors, "Email is required"); 
        }
        if (empty($this->user->getPassword())) { 
            array_push($errors, "Password is required"); 
        }
    }

    /* User email and password duplicates checking. Show errors if necessary */

    private function validateDuplicate($db, $name, $email, & $errors){
        $user_check_query = "SELECT * FROM users WHERE BINARY name='$name' OR BINARY email='$email' LIMIT 1";
        $result = $db->query($user_check_query);
        $dbUser = mysqli_fetch_object($result, 'User');
            
        if($dbUser){
            if ($dbUser->getName() === $name) {
                array_push($errors, "This name already exists");
            }

            if ($dbUser->getEmail() === $email) {
                array_push($errors, "This email already exists");
            }
        }
    }
}
?>