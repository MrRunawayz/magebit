<?php
session_start();

/* message in case of user wasn't logged in */

if (!isset($_SESSION['name'])) {
    $_SESSION['msg'] = "You must log in first";
    header('location: index.php');
}

/* message if logout link was clicked */

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['name']);
    header("location: index.php");
}
?>


<!DOCTYPE html>
<html>
<head>
    <title>Result</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>

<div class="content">
    <?php if (isset($_SESSION['success'])) : ?>
        <div class="error success" >
            <h3>
                <?php
                echo $_SESSION['success'];
                unset($_SESSION['success']);
                ?>
            </h3>
        </div>
    <?php endif ?>

    <?php  if (isset($_SESSION['email'])) : ?>
        <p class="logout"> <a href="result.php?logout='1'">logout</a> </p>
    <?php endif ?>

    <?php  if (isset($_SESSION['name'])) : ?>
        <p class="logout"> <a href="result.php?logout='1'">logout</a> </p>
    <?php endif ?>
</div>

</body>
</html>