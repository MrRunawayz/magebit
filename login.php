<?php

class Login {
    
    private $user;
    
    public function __construct($user){
        $this->user = $user;
    }

    /* connect to database, get values and login user. Login successful result is being showed in result.php */

    public function doAction(& $errors){
        $this->validateRequiredFields($errors);

        if(count($errors) == 0){
            $database = new Database('localhost', 'root', 'password', 'login');
            $db = $database->getLink();

            $email = $db->real_escape_string($this->user->getEmail());
            $password = $db->real_escape_string($this->user->getPassword());

            $query = "SELECT * FROM users WHERE email='$email' LIMIT 1";
            $result = $db->query($query);
            $dbUser = mysqli_fetch_object($result, 'User');

            if ($dbUser && password_verify($password, $dbUser->getPassword())) {
                $_SESSION['name'] = $dbUser->getName();
                $_SESSION['success'] = "You are now logged in!";
                header('location: result.php');
            }else {
                array_push($errors, "Wrong email and/or password");
            }

            $db->close();
        }
    }

    /* Empty fields checking */

    private function validateRequiredFields(& $errors){
        if (empty($this->user->getEmail())) { 
            array_push($errors, "Email is required"); 
        }
        if (empty($this->user->getPassword())) { 
            array_push($errors, "Password is required"); 
        }
    }
}
?>