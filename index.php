<?php include('action.php');?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Magebit</title>
    <script src="jquery-3.3.1.min.js"></script>
    <script src="animation.js"></script>
    <link href="styles.css" rel="stylesheet" type="text/css" media="screen">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300" rel="stylesheet">
</head>

<body>

<header><?php include('errors.php'); ?></header>

<div class="bg-image"></div>

    <div id="form" class="form">

        <div id="blue-bg" class="blue-bg">
            <div class="signup-text">
                <p class="headline">Don't have an account?</p>
                <div class="underline"></div><br>
                <div class="subtext">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </div>

                <br>
                <button id="signup_button" class="button">SIGN UP</button>
            </div>

            <div class="login-text">
                <p class="headline">Have an account?</p>
                <div class="underline"></div><br>
                <div class="subtext">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                </div>

                <br>
                <button id="login-button" class="button">LOGIN</button>
            </div>

        </div>

        <div id="border" class="border"></div>

        <div id="white-bg" class="white-bg">
            <div id="login-form" class="login-form">
                    <p class="headline">Login</p>
                    <div class="logo"></div>
                    <div class="underline"></div>
                <br>
                <form method="post" action="index.php">
                    <div class="form-row"><label for="email">Email:</label></div><div class="email-pic"><img src="images/ic_mail_unactive.png" id="login-email-grey-pic"/></div>
                    <div class="form-row"><input type="text" id="email" name="email"></div><br>

                    <div class="form-row"><label for="password">Password:</label></div><div class="password-pic"><img src="images/ic_lock_unactive.png" id="login-password-grey-pic"/></div>
                    <div class="form-row"><input type="password" id="password" name="password"></div>

                    <div class="form-row"><input class="login-button" name="login-button" type="submit" value="LOGIN"></div>
                </form>
                <div class="add-text">Forgot?</div>
            </div>

            <div id="signup-form" class="signup-form">
                <p class="headline">Sign Up</p>
                <div class="logo"></div>
                <div class="underline"></div>
                <br>
                <form method="post" action="index.php">
                    <div class="form-row"><label for="signup-name">Name:</label></div><div class="user-pic"><img src="images/ic_user_unactive.png" id="signup-user-grey-pic"/></div>
                    <div class="form-row"><input type="text" id="signup-name" name="name" ></div><br>

                    <div class="form-row"><label for="signup-email">Email:</label></div><div class="email-pic"><img src="images/ic_mail_unactive.png" id="signup-email-grey-pic"/></div>
                    <div class="form-row"><input type="text" id="signup-email" name="email"></div><br>

                    <div class="form-row"><label for="signup-pass">Password:</label></div><div class="password-pic"><img src="images/ic_lock_unactive.png" id="signup-password-grey-pic"/></div>
                    <div class="form-row"><input type="password" id="signup-pass" name="password"></div><br>

                    <div class="form-row"><input class="signup-button" name="signup-button" type="submit" value="SIGN UP"></div>
                </form>
            </div>
        </div>

    </div>

<footer>
    All rights reserved "Magebit" 2016.
</footer>

</body>
</html>