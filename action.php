<?php

include('database.php');
include('signup.php');
include('login.php');
include('user.php');

session_start();

$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$password = isset($_POST['password']) ? $_POST['password'] : '';
$errors = array();

$user = new User();
$user->setName($name);
$user->setEmail($email);
$user->setPassword($password);

if (isset($_POST['signup-button'])) {
    $signUp = new SignUp($user);
    $signUp->doAction($errors);
}

if (isset($_POST['login-button'])) {
    $login = new Login($user);
    $login->doAction($errors);
}

?>