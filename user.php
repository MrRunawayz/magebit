<?php
    
class User {
    private $name;
    private $email;
    private $password;

    public function __construct(){
        
    }
    
    public function getName(){
        return $this->name;
    }

    public function setName($name){
        $this->name = trim($name);
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = trim($email);
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($password){
        $this->password = trim($password);
    }
}
?>